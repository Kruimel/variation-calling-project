# doel: het maken van html files met de benigne varianten
# datum: 11-06-19
# auteur: Moniek van Selst

import os
import json

files = ["sample1","sample2","sample3"]

# Deze rule roept de andere rules aan.
# Als alle bastanden gemaakt zijn worden de jsons in het api mapje verwijderd
# en word een html file geopend in de browser.
rule all:
    input:
        expand("outputfiles/{sample}.benign.html",sample=files),
        expand("outputfiles/{sample}.change.html",sample=files)
    run:
        for sample in files:
            os.system("rm api/{}.perc.json".format(sample))
            os.system("rm api/{}.filterd.json".format(sample))
        os.system("xdg-open outputfiles/{}.benign.html ".format(sample))


# Deze rule vergelijkt per regel in het bestand of deze variant benign is.
# Als deze variant benign is word hij opgeslagen in een json.
# Deze json word opgeslagen in het api en het output mapje.
rule get_variants:
    input:
        a = 'inputfiles/{sample}.txt'
    output:
        b = 'outputfiles/{sample}.filterd.json',
        c = 'api/{sample}.filterd.json'
    run:
        output_json = {}
        out = open(output.b, "w+")
        out2 = open(output.c, "w+")
        for line in open(input.a):
            print(line)
            line = line.strip()
            words = line.split("\t")
            print(words)
            os.system("wget 'http://localhost:5001/db_request?chromosoom={}&locatie={}&alt={}'".format(words[0], words[1], words[2]))
            file = open("db_request?chromosoom={}&locatie={}&alt={}".format(words[0], words[1], words[2]))
            json_text = ""
            for line in file:
                json_text += line
            os.system("rm 'db_request?chromosoom={}&locatie={}&alt={}'".format(words[0], words[1], words[2]))
            json_Data = json.loads(json_text)
            print(json_Data['data'])
            if not json_Data['data']:
                print("leeg")
            else:
                print(json_Data['data'][0]['id'])
                output_json[str(json_Data['data'][0]['id'])] = json_Data['data'][0]
        out.write(str(output_json).replace("'",'"'))
        out2.write(str(output_json).replace("'",'"'))


# Deze rule gebruikt de json met benigne varianten in het api mapje.
# Van de json word een tabel gemaakt in een html file.
rule create_html:
    input:
        a = 'api/{sample}.filterd.json'
    output:
        b = 'outputfiles/{sample}.benign.html'
    run:
        input_json = ""
        for line in open(input.a):
            input_json += line
        print("sss")
        os.system("wget 'http://localhost:5001/createHTML?name={}&files={}'".format(output.b.split(".")[0].split("/")[1],files))
        print("aa")
        os.system("mv ./createHTML* './{}'".format(output.b))


# Deze rule gebruikt de json met benigne varianten in het api mapje.
# Voor elk type verandeing word een percentage berekend.
# Dit word opgeslagen in een json file in het api en output mapje.
rule getPerc:
    input:
        a = 'outputfiles/{sample}.filterd.json'
    output:
        b = 'outputfiles/{sample}.perc.json',
        c = 'api/{sample}.perc.json'
    run:
        input_json = ""
        for line in open(input.a):
            input_json += line
        os.system("wget 'http://localhost:5001/getPerc?name={}'".format(output.b.split(".")[0].split("/")[1]))
        os.system("mv ./getPerc* './{}'".format(output.b))
        os.system("cp './{}' './{}'".format(output.b,output.c))


# Deze rule gebruikt de json met verandergingen percentages in het api mapje.
# Van de json word een tabel gemaakt in een html file.
rule create_perc_html:
    input:
        a = 'api/{sample}.perc.json'
    output:
        b = 'outputfiles/{sample}.change.html'
    run:
        input_json = ""
        for line in open(input.a):
            input_json += line.strip()
        os.system("wget 'http://localhost:5001/createChangeHTML?name={}&files={}'".format(output.b.split(".")[0].split("/")[1],files))
        os.system("mv ./createChangeHTML* './{}'".format(output.b))
