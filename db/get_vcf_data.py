# generate sql dump from vcf data
# 11/06/2019
# Kim van Eijck

import vcf
import requests

# get data from gnomad
data = requests.get(
'https://storage.googleapis.com/gnomad-public/release/2.1.1/vcf/exomes/gnomad.exomes.r2.1.1.sites.X.vcf.bgz')

vcf_reader = vcf.Reader(data)
# csv writer: https://stackoverflow.com/questions/45513327/python-writing-to-csv-files-and-for-loops
with open("setup_vcf_db.sql", "w") as sql_file:
    sql_file.write("create table main_table (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, chromosoom CHAR(2), locatie INT, ref_nt CHAR(100), alt_nt CHAR(100), frequentie FLOAT);" + "\n")
    counter = 0
    for record in vcf_reader:
        counter += 1
        if counter % 10 == 0:
            print(counter)
        try:
            if record.INFO['AF'][0] < 0.01:
                sql_file.write("insert into main_table (chromosoom, locatie, ref_nt, alt_nt, frequentie) values ('%s', %d, '%s', '%s', %f);"%(record.CHROM, record.POS, record.REF, record.ALT[0], record.INFO['AF'][0]) + "\n")
        except:
            print("fout")
            pass
