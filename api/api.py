# Based on: https://www.codementor.io/sagaragarwal94/building-a-basic-restful-api-in-python-58k02xsiq
# Moniek van Selst & Kim van Eijck
# 11/06/2019

from flask import Flask, jsonify, request
from flaskext.mysql import MySQL
import json

app = Flask(__name__)

# MySQL configurations
mysql = MySQL()
mysql.init_app(app)
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'variant_db'
app.config['MYSQL_DATABASE_HOST'] = 'db'

# test flask
@app.route('/')
def hello_world():
    return 'Flask Dockerized'


# test: get everything from the variant table
@app.route('/request_test')
def request_test():
    cur = mysql.connect().cursor()
    cur.execute('''select * from variant_db.main_table''')
    r = [dict((cur.description[i][0], value)
              for i, value in enumerate(row)) for row in cur.fetchall()]
    return jsonify({'data': r})


# gets variant out of database if frequency >1%
@app.route('/db_request')
def db_request():
     # voorbeeld: http://localhost:5001/db_request?chromosoom=15&locatie=20170177&alt=G
    chromosoom = request.args.get('chromosoom')
    locatie = request.args.get('locatie')
    alt = request.args.get('alt')
    sqlStatement = "SELECT * FROM main_table WHERE chromosoom={} AND locatie={} AND alt_nt='{}' AND frequentie<0.01".format(
        chromosoom, locatie, alt)
    cur = mysql.connect().cursor()
    cur.execute(sqlStatement)
    r = [dict((cur.description[i][0], value)
              for i, value in enumerate(row)) for row in cur.fetchall()]
    return jsonify({'data': r})


# creates HTML page with variants that were found
@app.route('/createHTML')
def createHTML():
    try:
        # voorbeeld: http://localhost:5001/createHTML?name=sample1
        # inputfile = request.args.get('inputjson')
        name = request.args.get('name')
        samples = request.args.get('files')[1:-1].split(",")
        filemenu = "<ul>"
        for sample in samples:
            if name in sample:
                filemenu += '<li><a class="active" href="{}.binigne.html">{}</a></li>'.format(
                    sample, sample)
            else:
                filemenu += '<li><a href="{}.binigne.html">{}</a></li>'.format(
                    sample, sample)
        filemenu += "</ul>"

        file = open(name + ".filterd.json")
        jsonData = ""
        for line in file:
            jsonData += line
        html = ""
        for line in open("base.html"):
            print(str(line))
            if "text =" in str(line):
                print("cccc")
                html += "      text ='" + jsonData + "'\n"
            # elif "<ul></ul>" in str(line):
            # html += filemenu
            else:
                regel = str(line).replace(
                    "sample", name).replace("<ul></ul>", filemenu)
            # regel = str(regel).replace("<ul></ul>",filemenu)
                print("BBb")
                html += regel
        return html
    except Exception as e:
        print(e)


# gets percetage in which a certain varation happens
@app.route('/getPerc')
def getPerc():
    try:
        # voorbeeld: http://localhost:5001/createHTML?name=sample1
        name = request.args.get('name')
        file = open(name + ".filterd.json")
        jsonData = ""
        for line in file:
            jsonData += line
        json_Data = json.loads(jsonData)
        alle = {}
        totaal = 0
        for snip in json_Data:
            totaal += 1
            verandering = json_Data[snip]["ref_nt"] + \
                "->" + json_Data[snip]["alt_nt"]
            if verandering in alle.keys():
                alle[verandering]["aantal"] += 1
            else:
                alle[verandering] = {}
                alle[verandering]["aantal"] = 1

        for key in alle:
            key2 = key.split("->")
            alle[key]["referentie"] = key2[0]
            alle[key]["alternatief"] = key2[1]
            alle[key]["percentage"] = alle[key]["aantal"] * 100 / totaal

        return jsonify(alle)
    except Exception as e:
        print(e)


@app.route('/createChangeHTML')
def createChangeHTML():
    try:
        # voorbeeld: http://localhost:5001/createHTML?name=api/sample1.perc.json
        name = request.args.get('name')
        file = open(name + ".perc.json")
        samples = request.args.get('files')[1:-1].split(",")
        filemenu = "<ul>"
        for sample in samples:
            if name in sample:
                filemenu += '<li><a class="active" href="{}.change.html">{}</a></li>'.format(
                    sample, sample)
            else:
                filemenu += '<li><a href="{}.change.html">{}</a></li>'.format(
                    sample, sample)
        filemenu += "</ul>"

        jsonData = ""
        for line in file:
            jsonData += line.strip()
        html = ""
        for line in open("baseChange.html"):
            print(str(line))
            if "text =" in str(line):
                html += "      text ='" + jsonData + "'\n"
            else:
                regel = line.replace("sample", str(name)).replace(
                    "<ul></ul>", filemenu)
                print("BBb")
                html += regel
        return html
    except Exception as e:
        print(e)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
