# Variation calling project
# 11/06/2019
# Moniek van Selst & Kim van Eijck

This application consists of 2 Docker containers that can be started with 'docker-compose up -d'
The Flask api runs in one container and is able to get data from the mysql database that is running in the other container
Using 'snakemake' the input files can be analysed with a pipeline which generates a html file with the results
